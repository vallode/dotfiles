#!/bin/bash

rofi_command="rofi -theme $HOME/.config/rofi/themes/sysmenu.rasi"
uptime=$(uptime -p | sed -e 's/up //g')

options="> lock\n> logout\n> reboot\n> shutdown"
CHOICE="$(echo -e "$options" | $rofi_command -p "uptime: $uptime" -dmenu)"

case $CHOICE in
    *lock) dm-tool switch-to-greeter ;;
    *logout) i3-msg exit ;;
    *reboot) systemctl reboot ;;
    *shutdown) systemctl -i poweroff
esac