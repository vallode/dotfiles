; File Inclusion
include-file = ~/.config/polybar/colors
include-file = ~/.config/polybar/modules

[bar/main]
; Require the monitor to be in connected state
; XRandR sometimes reports my monitor as being disconnected (when in use)
monitor-strict = false

; Tell the Window Manager not to configure the window.
; Use this to detach the bar if your WM is locking its size/position.
override-redirect = false

; Put the bar at the bottom of the screen
bottom = false

; Prefer fixed center position for the `modules-center` block
; When false, the center position will be based on the size of the other blocks.
fixed-center = true

; Dimension defined as pixel value (e.g. 35) or percentage (e.g. 50%),
; the percentage can optionally be extended with a pixel offset like so:
; 50%:-10, this will result in a width or height of 50% minus 10 pixels
width = 100%
height = 30 

background = ${color.bg}
foreground = ${color.fg}

; Under-/overline pixel size and argb color
; Individual values can be defined using:
;   {overline,underline}-size
;   {overline,underline}-color
overline-size = 4
overline-color = ${color.accent}

padding = 0

; Number of spaces to add before/after each module
; Individual side values can be defined using:
;   module-margin-{left,right}
module-margin-left = 0
module-margin-right = 0

font-0 = "Inconsolata:bold:size=11;2"
font-1 = "Siji:size=12;2"

; Modules are added to one of the available blocks
;   modules-left = cpu ram
;   modules-center = xwindow xbacklight
;   modules-right = ipc clock

;; Available modules
;;
;alsa backlight battery
;bspwm cpu date
;filesystem github i3
;memory mpd wired-network
;network pulseaudio temperature
;keyboard title workspaces
;;
;; User modules
;checknetwork updates window_switch launcher powermenu sysmenu menu style
;;
;; Bars
;cpu_bar memory_bar filesystem_bar mpd_bar 
;volume brightness battery_bar 

modules-left = separator_b i3 separator_b
modules-center = date
modules-right = network_i network separator backlight_i backlight alsa_i alsa separator battery separator powermenu

; The separator will be inserted between the output of each module
separator =

; Locale used to localize various module data (e.g. date)
; Expects a valid libc locale, for example: sv_SE.UTF-8
locale = 

tray-position = center
tray-detached = false
tray-maxsize = 16
tray-background = ${color.bg}
tray-offset-x = 325 
tray-offset-y = 0
tray-padding = 0
tray-scale = 1.0

; Enable support for inter-process messaging
; See the Messaging wiki page for more details.
enable-ipc = true

; Fallback click handlers that will be called if
; there's no matching module handler found.
click-left = 
click-middle = 
click-right =
double-click-left =
double-click-middle =
double-click-right =

; Requires polybar to be built with xcursor support (xcb-util-cursor)
; Possible values are:
; - default   : The default pointer as before, can also be an empty string (default)
; - pointer   : Typically in the form of a hand
; - ns-resize : Up and down arrows, can be used to indicate scrolling
cursor-click = 
cursor-scroll = 

; i3 Workspace Specific
; scroll-up = i3wm-wsnext
; scroll-down = i3wm-wsprev
scroll-up = i3-msg workspace next_on_output
scroll-down = i3-msg workspace prev_on_output

[settings]
; The throttle settings lets the eventloop swallow up til X events
; if they happen within Y millisecond after first event was received.
; This is done to prevent flood of update event.
;
; For example if 5 modules emit an update event at the same time, we really
; just care about the last one. But if we wait too long for events to swallow
; the bar would appear sluggish so we continue if timeout
; expires or limit is reached.
throttle-output = 5
throttle-output-for = 10

; Time in milliseconds that the input handler will wait between processing events
throttle-input-for = 30

; Reload upon receiving XCB_RANDR_SCREEN_CHANGE_NOTIFY events
screenchange-reload = false

; Compositing operators
; @see: https://www.cairographics.org/manual/cairo-cairo-t.html#cairo-operator-t
compositing-background = source
compositing-foreground = over
compositing-overline = over
compositing-underline = over
compositing-border = over

; Define fallback values used by all module formats
format-foreground = 
format-background = 
format-underline =
format-overline =
format-spacing =
format-padding =
format-margin =
format-offset =

; Enables pseudo-transparency for the bar
; If set to true the bar can be transparent without a compositor.
pseudo-transparency = false
