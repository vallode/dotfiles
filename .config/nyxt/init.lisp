;; Add blocker mode to default buffer modes.
(define-configuration buffer
  ((default-modes (append '(blocker-mode) %slot-default))))

;; Sets the default buffer URL
(define-configuration buffer 
  ((default-new-buffer-url "file:///home/vallode/.config/startpage/index.html")))


