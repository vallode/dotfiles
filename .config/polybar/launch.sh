#!/bin/bash

pkill polybar

while pgrep -x polybar >/dev/null; do sleep 1; done

polybar main &

echo "Polybar launched"
