#!/usr/bin/env bash

if [ ! -z "$@" ]
then
  QUERY=$@
  recoll -t -e -b ${QUERY}
else
  echo "Finding files"
fi
