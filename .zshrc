# See: https://github.com/ohmyzsh/ohmyzsh/blob/master/templates/zshrc.zsh-template

# Path to your oh-my-zsh installation.
export PATH=$HOME/bin:/usr/local/bin:$PATH
export ZSH=$HOME/.oh-my-zsh

# PATH extensions
export PATH=$PATH:/usr/bin/elixir
export PATH=$PATH:/home/vallode/Documents/nvidia-xrun
export PATH=$PATH:/home/vallode/.cargo/bin
export PATH=$PATH:/home/vallode/.local/bin
export PATH=$PATH:/opt/firefox
export PATH=$PATH:/home/vallode/go/bin
export PATH=$PATH:/home/vallode/.pyenv/bin
export PATH=$PATH:/home/vallode/Temp/aseprite/build/bin

fpath+=~/.zfunc

# Set name of the theme to load.
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="kardan"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
HYPHEN_INSENSITIVE="true"

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git python debian elixir)

source $ZSH/oh-my-zsh.sh

source /opt/conda/etc/profile.d/conda.sh

# User configuration

export LANG=en_GB.UTF-8

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

source $HOME/.zsh-nvm/zsh-nvm.plugin.zsh

alias sai='sudo apt-get install'
alias sau='sudo apt-get update'
alias spy='source env/bin/activate'
alias ls='ls --color=auto --size --human-readable --kibibytes -X'
alias lsd='ls --color=auto -d $PWD/*'
alias xa='xrandr --auto'
alias config='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
alias note='vi $(date +%F)'
alias ytdl='youtube-dl -f bestaudio\[ext=m4a\] --embed-thumbnail --add-metadata'

export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"

eval "$(direnv hook zsh)"

# Storing last cd location
export PROMPT_COMMAND="pwd > /tmp/whereami"
precmd() { eval "$PROMPT_COMMAND" }

export PATH="$HOME/.poetry/bin:$PATH"
