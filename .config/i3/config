# Please see https://i3wm.org/docs/userguide.html for a complete reference!

set $mod Mod4
floating_modifier $mod

# Main font
font pango:Public Sans SemiBold 12

# Press same workspace to go back to the previous one
workspace_auto_back_and_forth yes

# Start a terminal
bindsym $mod+Return exec i3-sensible-terminal 
bindsym $mod+Shift+Return exec i3-sensible-terminal -cd `cat /tmp/whereami` 

# Kill the focused window
bindsym $mod+Shift+q kill

# Sticky the focused window
bindsym $mod+Shift+s sticky toggle

# Start rofi
# (general, logout, find)
bindsym $mod+space exec "rofi -combi-modi window,drun -show combi -modi combi"
bindsym $mod+q exec $HOME/.config/rofi/scripts/sysmenu.sh
bindsym $mod+Shift+f exec "rofi -show find"

# Screenshots using Maim
bindsym $mod+Shift+F1 exec filepath=$(echo ~/Pictures/Screenshots/$(date +%s).png) && maim -u -s $filepath && xclip -selection clipboard -t image/png $filepath
bindsym $mod+Shift+F2 exec filepath=$(echo ~/Pictures/Screenshots/$(date +%s).png) && maim -u -i $(xdotool getactivewindow) $filepath && xclip -selection clipboard -t image/png $filepath

# Change focus
bindsym $mod+j focus left
bindsym $mod+k focus down
bindsym $mod+l focus up
bindsym $mod+semicolon focus right

bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# Move focused window
bindsym $mod+Shift+j move left
bindsym $mod+Shift+k move down
bindsym $mod+Shift+l move up
bindsym $mod+Shift+semicolon move right

bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# Split in horizontal orientation
bindsym $mod+h split h

# Split in vertical orientation
bindsym $mod+v split v

# Fullscreen focused window
bindsym $mod+f fullscreen toggle

bindsym $mod+Shift+m [instance="cmus"] focus; fullscreen toggle 

# Toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# Workspace management
# TODO: Find a way to get rid of this mess...
set $ws1 "0"
set $ws2 "1"
set $ws3 "2"
set $ws4 "3"
set $ws5 "4"
set $ws6 "5"
set $ws7 "6"
set $ws8 "7"
set $ws9 "8"
set $ws10 "9"

bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace $ws6
bindsym $mod+7 workspace $ws7
bindsym $mod+8 workspace $ws8
bindsym $mod+9 workspace $ws9
bindsym $mod+0 workspace $ws10

bindsym $mod+Shift+1 move container to workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6
bindsym $mod+Shift+7 move container to workspace $ws7
bindsym $mod+Shift+8 move container to workspace $ws8
bindsym $mod+Shift+9 move container to workspace $ws9
bindsym $mod+Shift+0 move container to workspace $ws10

# Automatically move dialog windows to the mouse
for_window [window_type="Dialog"] move position mouse

# Workspace assignment
focus_on_window_activation urgent
# assign [class="Slack"] $ws1

# Reload the configuration file
bindsym $mod+Shift+c reload

# Restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart

# Exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -B 'Yes, exit i3' 'i3-msg exit'"

# Set simple colors
set $bg #f9faf4
set $fg #32322a
set $accent #df7d60
set $red #843939

# Class                 border|backgr|text|indicator|child_border
client.focused          $accent $accent $fg $accent $accent
client.focused_inactive $accent $accent $fg $accent $accent
client.unfocused        $bg $bg $fg $bg $bg
client.urgent           $accent $red $bg $red $red

client.background       $bg

# Window configurations 
for_window [class=".*"] border pixel 4
for_window [class=".*mpv.*"] sticky enable
for_window [class=".*Audacious.*"] sticky enable

# i3 Gaps
gaps outer 12 
gaps inner 12 

# Set random wallpaper 
# exec_always --no-startup-id "feh --bg-tile --no-fehbg $HOME/Pictures/Wallpapers/pixel.png"
exec_always --no-startup-id "feh --bg-fill --no-fehbg $HOME/Pictures/Wallpapers/forest.png"
exec_always --no-startup-id $HOME/.config/polybar/launch.sh
exec_always --no-startup-id dunst -config $HOME/.config/dunst/dunstrc

# Execute once per boot
exec --no-startup-id picom --experimental-backends -b
exec --no-startup-id thunar --daemon

