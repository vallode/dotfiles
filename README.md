# dotfiles

Attempting to store my dotfiles here for posterity...

## Setting up  
I've attempted to find the simplest method (through my utter hatred of dependencies) to make a dotfiles repository. The [bare git repository](https://news.ycombinator.com/item?id=11070797) method seems to tick all my boxes.

```bash
git init --bare $HOME/.dotfiles
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
config config --local status.showUntrackedFiles no
```

You should then add the alias to config into your respective *rc file.

## Cloning

```bash
git clone --bare <repo-url> $HOME/.dotfiles
alias config='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
config checkout
```

## Sources  
Thanks to these great posts for helping me understand this method:  
https://www.atlassian.com/git/tutorials/dotfiles  
https://news.ycombinator.com/item?id=11070797  
https://wiki.archlinux.org/index.php/Dotfiles  
